package pl.atk.pingpong.controllers

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest
@RunWith(SpringRunner::class)
class PingPongControllerTest {

    val client = WebTestClient.bindToRouterFunction (PingPongController().router()).build()

    @Test
    fun pingTest() {
        client
                .get()
                .uri("/ping")
                .exchange()
                .expectStatus()
                .isOk
                .expectBody()
                .json("""
                    {
                        "message": "ping",
                        "status": "ok",
                        "errors": []
                    }
                """)

    }

    @Test
    fun pongTest() {
        client
                .get()
                .uri("/pong")
                .exchange()
                .expectStatus()
                .isOk
                .expectBody()
                .json("""
                    {
                        "message": "pong",
                        "status": "ok",
                        "errors": []
                    }
                """)

    }

}
