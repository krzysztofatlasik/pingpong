package pl.atk.pingpong.controllers

import com.sun.security.ntlm.Server
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatus.*
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.web.reactive.function.BodyInserters.fromObject
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router
import pl.atk.pingpong.responses.PingPongResponse
import reactor.core.publisher.Mono
import javax.annotation.PostConstruct

@Configuration
class PingPongController {

    fun createPingPongResponse(message: String): (ServerRequest) -> Mono<ServerResponse> {
        return {
            ServerResponse
                    .ok()
                    .body(
                        fromObject(PingPongResponse("ok", message, emptyList())
                    )
            )
        }
    }

    @Bean
    fun router() = router {
        GET("/ping", createPingPongResponse("ping"))
        GET("/pong", createPingPongResponse("pong"))
        GET("/nf") { ServerResponse.status(NOT_FOUND).body(
                fromObject(PingPongResponse("notFound", "", listOf("Resource was not found")))
        ) }
    }
}