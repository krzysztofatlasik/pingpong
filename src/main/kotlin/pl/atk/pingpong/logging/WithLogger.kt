package pl.atk.pingpong.logging

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.util.ClassUtils

interface WithLogger {

    val log: Logger
        get() = LoggerFactory.getLogger(
                ClassUtils.getUserClass(this::class.java)
        )


}

