package pl.atk.pingpong.responses

data class PingPongResponse(
        val status: String,
        val message: String,
        val errors: List<String>
) {

}