package pl.atk.pingpong

import org.springframework.context.annotation.Configuration
import pl.atk.pingpong.logging.WithLogger
import javax.annotation.PostConstruct

@Configuration
class InitializingInfoLogger(val  serverProperties: ServerProperties) : WithLogger {

    @PostConstruct
    fun logInfo() {
        log.info("Using server port: {}", serverProperties.port)
    }


}