FROM gradle:4.3.0-jdk8 AS BUILD

VOLUME /root/.gradle

RUN mkdir /home/gradle/workspace && chmod -R 777 /home/gradle/workspace
WORKDIR /home/gradle/workspace
COPY src src
COPY build.gradle build.gradle

RUN gradle --no-daemon build

FROM openjdk:jre-slim
MAINTAINER katlasik<krzysztof.atlasik@protonmail.com>

WORKDIR /opt/pingpong
COPY --from=BUILD /home/gradle/workspace/build/libs/*.jar .
RUN ln -s *.jar pingpong.jar

CMD ["java", "-jar", "pingpong.jar"]